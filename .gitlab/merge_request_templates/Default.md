## What does this MR do and why?

<!--
Describe in detail what your merge request does and why.
-->

<!--
## Screenshots or screen recordings
-->

## How to set up and validate locally

_Numbered steps to set up and validate the change are strongly suggested._

<!-- template sourced from https://gitlab.com/gitlab-org/cloud-native/charts/gitlab-zoekt/-/blob/main/.gitlab/merge_request_templates/Default.md -->

/assign me
/label ~"group::global search"
